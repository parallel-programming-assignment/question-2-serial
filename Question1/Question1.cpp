﻿// Question1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

void Question2() {
	cout << "This is the function Parallel Programming Assignment, Question 2 b that is serial programming for trapezium rule" << endl;
	float interval = 0.5, startPoint = 0, endPoint = 3, result = 0;
	cout << "This function (square root of (2x + 1)) run with parameters:\n" <<
		"Interval: " << interval << "\n" <<
		"Start point: " << startPoint << "\n" <<
		"End point: " << endPoint << endl;
	for (float x = startPoint; x < endPoint; x += interval) {
		float value = (sqrt(pow(2.0, x) + 1) + sqrt(pow(2.0, x + interval) + 1)) * 0.5 * 0.5;
		result += value;
	}
	cout << "The approximate sum is " << result << endl;
}

int main()
{
	Question2();
}